package cureForDiabetes;

import cureForDiabetes.prescribers.*;

public class DoctorVisit {
    private static MedicationPrescriber getChainOfPrescribers(){
        MedicationPrescriber humalogMix50Prescriber = new HumalogMix50Prescriber();
        MedicationPrescriber humalogPrescriber = new HumalogPrescriber();
        MedicationPrescriber novomixPrescriber = new NovomixPrescriber();
        MedicationPrescriber humulinM3Prescriber = new HumulinM3Prescriber();

        humalogMix50Prescriber.setNext(humulinM3Prescriber);
        humulinM3Prescriber.setNext(novomixPrescriber);
        novomixPrescriber.setNext(humalogPrescriber);

        return humalogMix50Prescriber;
    }

    public static void main(String[] args) {
        Patient patient = new Patient();
        patient.name = "Vasiliy";
        patient.age = 68;
        patient.anamnesis.isResponsible = true;
        patient.anamnesis.isDriver = false;
        patient.anamnesis.hasAllergyToHumalog = true;
        patient.anamnesis.hasAspartHypersensitivity = false;

        MedicationPrescriber prescriber = getChainOfPrescribers();
        patient.medication = prescriber.prescribe(patient.anamnesis);
        System.out.println(patient.medication);
    }
}
