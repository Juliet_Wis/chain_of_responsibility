package cureForDiabetes.prescribers;

import cureForDiabetes.Anamnesis;

public class HumulinM3Prescriber extends MedicationPrescriber{
    @Override
    public String prescribe(Anamnesis anamnesis) {
        if (!anamnesis.isDriver)
            return "HumulinM3";
        if (next != null) return next.prescribe(anamnesis);
        return "";
    }
}
