package cureForDiabetes.prescribers;

import cureForDiabetes.Anamnesis;

public class HumalogMix50Prescriber extends MedicationPrescriber {
    @Override
    public String prescribe(Anamnesis anamnesis) {
        if (!anamnesis.isResponsible)
            return "Humalog Mix 50";
        if (next != null) return next.prescribe(anamnesis);
        return "";
    }
}
