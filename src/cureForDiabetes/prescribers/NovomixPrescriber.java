package cureForDiabetes.prescribers;

import cureForDiabetes.Anamnesis;

public class NovomixPrescriber extends MedicationPrescriber {
    @Override
    public String prescribe(Anamnesis anamnesis) {
        if (anamnesis.hasAspartHypersensitivity)
            return "Novomix 30/7";
        if (next != null) return next.prescribe(anamnesis);
        return "";
    }
}
