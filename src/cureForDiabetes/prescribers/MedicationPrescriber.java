package cureForDiabetes.prescribers;

import cureForDiabetes.Anamnesis;

public abstract class MedicationPrescriber implements Prescriber{
    protected Prescriber next;

    @Override
    public void setNext(Prescriber prescriber){
        this.next = prescriber;
    }

    public abstract String prescribe(Anamnesis anamnesis);
}
