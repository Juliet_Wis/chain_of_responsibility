package cureForDiabetes.prescribers;

import cureForDiabetes.Anamnesis;

public interface Prescriber {
    void setNext(Prescriber prescriber);
    String prescribe(Anamnesis anamnesis);
}
