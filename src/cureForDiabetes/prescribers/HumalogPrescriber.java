package cureForDiabetes.prescribers;

import cureForDiabetes.Anamnesis;

public class HumalogPrescriber extends MedicationPrescriber {
    @Override
    public String prescribe(Anamnesis anamnesis) {
        if (!anamnesis.hasAllergyToHumalog)
            return "Humalog";
        if (next != null) return next.prescribe(anamnesis);
        return "";
    }
}
